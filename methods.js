const fetchImages = require('./fetchImages.js')
const fetchFonts = require('./fetchFonts.js')

module.exports = {
    fetchImages: fetchImages,
    fetchFonts: fetchFonts
}