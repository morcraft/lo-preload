const fetchSources = require('./fetchSources.js')
const EventsEmitter = require('events')
const _ = require('lodash')

module.exports = function(args){
    var self = this

    self.eventsEmitter = new EventsEmitter()

    self.fetchSources = function(_args){
        return fetchSources(_.merge({
            eventsEmitter: self.eventsEmitter
        }, _args))
    }
}