const checkTypes = require('check-types')
const FontFaceObserver = require('fontfaceobserver')
const _ = require('lodash')

module.exports = function(source, options){
    if(!_.isObject(options))
        options = {}
    
    checkTypes.assert.string(source)

    var font = new FontFaceObserver(source)
    if(checkTypes.not.string(options.testString))
        options.testString = 'A test string'

    if(checkTypes.not.number(options.timeout))
        options.timeout = 720000//12 minutes

    return font.load(options.testString, options.timeout)
}